# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

## Conteneurisation du projet 

1. Builder l'image de l'application grâce aux instructions du Dockerfile situé à la racine du projet :
   ````sh
   docker image build -t hello-word .
   
   # Sortie :
    => [builder 5/6] COPY src ./src                                                                                                                                                   0.1s 
    => [builder 6/6] RUN mvn clean -e -B package                                                                                                                                     24.8s
    => [stage-1 3/3] COPY --from=builder /app/target/simple-http-hello-world-1.0-SNAPSHOT.jar .                                                                                       0.1s
    => exporting to image                                                                                                                                                             0.2s
    => => exporting layers                                                                                                                                                            0.1s
    => => writing image sha256:c2c81fe46a95668e93d699130f763d53a37111bd35606e874a7a1571bae68de6                                                                                       0.0s
    => => naming to docker.io/library/hello-word 
    ````
2. Vérifier que l'image existe dans Docker desktop (nom image : hello-world) ou directement en ligne de commande (nom image : docker.io/library/hello-word):
    ````sh
    docker inspect docker.io/library/hello-word
    # ou
    docker inspect hello-word
   
    # sortie (1ères lignes):
     {
        "Id": "sha256:c2c81fe46a95668e93d699130f763d53a37111bd35606e874a7a1571bae68de6",
        "RepoTags": [
            "hello-word:latest"
        ],
        "RepoDigests": [],
        "Parent": "",
        "Comment": "buildkit.dockerfile.v0",
        "Created": "2022-04-15T07:53:45.9985299Z",
        "Container": "",
     (.......)
    }
    ````
3. Lancer le container :
    ````sh
    docker container run -d -p 80:80  hello-word
    # Sortie sha du container :
    4658427d6c8e4e7608cfccacbb0d7d6007533bcffb7e58a3dab1e102ebe1d2c8

    ````
4. Vérifier que le conteneur existe et tourne, dans docker desktop ou en ligne de commande :
    ````sh
    docker container ls -a
    # ou
    docker container ps -a
   
    # Sortie : 
    CONTAINER ID   IMAGE                            COMMAND                  CREATED          STATUS                      PORTS                  NAMES
   3fd7bdda80f6   hello-world                      "/hello"                 12 minutes ago   Exited (0) 12 minutes ago                          gracious_jang
   3e06edce22d9   hello-word                       "java -jar ./simple-…"   24 minutes ago   Exited (1) 13 minutes ago                          determined_chandrasekhar
   4a1afe0da2eb   wordle-reverse-proxy_wordle      "java -jar ./wordle-…"   2 weeks ago      Exited (255) 2 weeks ago                           wordle-reverse-proxy_wordle_1
   228901b75549   wordle-reverse-proxy_wordlebis   "java -jar ./wordle-…"   2 weeks ago      Exited (255) 2 weeks ago                           wordle-reverse-proxy_wordlebis_1
   f5233b7e997f   wordle-reverse-proxy_nginx       "/docker-entrypoint.…"   2 weeks ago      Exited (255) 2 weeks ago    0.0.0.0:8090->80/tcp   wordle-reverse-proxy_nginx_1
   c1efaf808007   scrabbleimage                    "java -jar app.jar"      2 weeks ago      Exited (255) 2 weeks ago                           wordle-reverse-proxy_scrabble_1
   1d3f5a8c64fa   nginx   
    ````